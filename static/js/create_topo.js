!(function(){
    "use strict";

    var graph;
    var graph_data;

    main();
            
    /**
     * Main function of the script
     */
    function main() {

        $("#scan_network").on("click", function(e) {

            // Search nodes of the current network
            $.getJSON( "/static/network/nodestates.json", function( json ) {

                graph_data = json;
                getScriptsList();
                drawGraph();

                $( document ).ready(function() {
                    
                    // Deploy a script on a specific node
                    $(".deployScript").on("click", function(e) {

                        var script = $(this).parent().parent().find('select').val()
                        var node_ip = $(this).parent().parent().data('ip');
                        deployScript( script, node_ip );

                    });

                    // Export all graph into a GML file
                    $("#exportJSONtoGML").on("click", function(e) {

                        exportJSONtoGML()

                    });
                });
            });

        });

    }

    /**
     * Get all Bash scripts list
    */
    function getScriptsList( ) {
        
        $.ajax({
            type: 'POST',
            url: '/getAllBashScripts',
            success: function(response){
                fillDataArray(response);
            },
        });

    }

    /**
     * Deploy script
    */
    function deployScript( script_name, node_ip ) {
        
        $.ajax({
            type: 'POST',
            url: '/deployScript',
            dataType: "json",
            data: {
                script: script_name,
                node: node_ip,
            },
        }).done(function(data, textStatus, jqXHR){
            if ( textStatus === 'success' ) {
                alert( 'Script déployé avec succès' );
            }
        }).fail(function(data){
            alert('error with the deployed script !');
        });

    }

    /**
     * export JSON to GML
    */
    function exportJSONtoGML() {

        // We have to clean the current graph to remove some useless data and avoid complexity
        var cleaned_graph = JSON.parse(JSON.stringify(graph_data));

        cleaned_graph.nodes.forEach( function( item ) {
            delete item["fixed"];
            delete item["weight"];
            delete item["index"];
            delete item["px"];
            delete item["py"];
            delete item["x"];
            delete item["y"];
        });

        // Fix fatal error on the conversion
        cleaned_graph.links.forEach( function( item ) {
            item.source = item.source["id"]
            item.target = item.target["id"]
        });

        $.ajax({
            type: 'POST',
            url: '/exportJSONtoGML',
            dataType: "text",
            data: {
                json_obj: JSON.stringify( cleaned_graph ),
            },
            success: function(gml_filename){
                // Download the GML file
                window.location = '/uploads/' + gml_filename
            },
            error: function( jqXHR, textStatus, errorThrown ) {
                alert('failed converting JSON to GML.');
                console.log( textStatus );
                console.log( errorThrown );
                console.log( jqXHR );
            },

        });

    }

    /**
     * Create table with nodes data
     */
    function fillDataArray( list ) {

        // Create the scripts list in the DOM
        var options = "";
        list.forEach( function( item ) {
            options += "<option value='" + item + "'>" + item + "</option>\n";
        });

        var select = "<select class='custom-select selectScript'>\n" +
                      options +
                    "</select>" +
                    "<button class='btn btn-sm btn-primary deployScript'>Déployer</button>";

        graph_data.nodes.forEach( function( item, index ) {
            var $tr = $('<tr>').attr('data-id', item.id).attr('data-ip', item.ip).append(
                $('<td>').text(item.id),
                $('<td>').text(item.ip),
                $('<td>').text(item.mac),
                $('<td>').text(item.latence),
                $('<td>').html(select)
            ).appendTo('#data table.nodes tbody');
        });
    }

    // because of the way the network is created, nodes are created first, and links second,
    // so the lines were on top of the nodes, this just reorders the DOM to put the svg:g on top
    function keepNodesOnTop() {
        $(".nodeStrokeClass").each(function( index ) {
            var gnode = this.parentNode;
            gnode.parentNode.appendChild(gnode);
        });
    }
    function addNodes() {
        d3.select("svg")
                .remove();
        
        drawGraph();
    }

    function drawGraph() {

        graph = new topologyGraph();
        keepNodesOnTop();
        
    }

    /**
     * Handle graph with nodes & links data
     */
    function topologyGraph() {

        var clicked_nodes = [];

        // Fix strange bug with D3js when we have some default links
        graph_data['links'] = [];

        // Add a node on the graph object
        var addNode = function (id) {
            graph_data.nodes.push({"id": id});
            update();
        };

        // Remove a node on the graph
        var removeNode = function (id) {
            var i = 0;
            var n = findNode(id);
            while (i < graph_data.links.length) {
                if ((graph_data.links[i]['source'] == n) || (graph_data.links[i]['target'] == n)) {
                    graph_data.links.splice(i, 1);
                }
                else i++;
            }
            graph_data.nodes.splice(findNodeIndex(id), 1);
            update();
        };

        // Remove all nodes of the graph
        var removeAllNodes = function () {
            graph_data.nodes.splice(0, graph_data.nodes.length);
            update();
        };

        // Remove all links of the graph
        var removeallLinks = function () {
            graph_data.links.splice(0, graph_data.links.length);
            update();
        };

        // Add a link on the graph
        var addLink = function(source, target) {
            graph_data.links.push({"source": findNode(source), "target": findNode(target) });
            update();
        };

        // Remove a link of the graph
        var removeLink = function (source, target) {
            for (var i = 0; i < graph_data.links.length; i++) {
                if (graph_data.links[i].source.id == source && graph_data.links[i].target.id == target) {
                    graph_data.links.splice(i, 1);
                    break;
                }
            }
            update();
        };

        // Display a pointer cursor at hover state, and show IP address of the node
        var hovered = function(d) {
            d3.select(this).style("cursor", "pointer");
            $('circle#'+d.id).tooltip({
                title: d.ip
            });
        };

        // When 2 nodes are clicked, we create a new link between them
        var clicked = function(d) {

            if ( clicked_nodes.length == 0 || ( clicked_nodes.length < 2 && clicked_nodes[0] != d.id ) ) {
                clicked_nodes.push( d.id );
            }

            if ( clicked_nodes.length == 2 ) {

                var flag = false;
                graph_data.links.forEach( function( item ) {
                    if ( item.source.id == clicked_nodes[0] && item.target.id == clicked_nodes[1] 
                        || item.source.id == clicked_nodes[1] && item.target.id == clicked_nodes[0] ) {
                        flag = true;
                    }
                });

                if ( flag == false ) {
                    addLink( clicked_nodes[0], clicked_nodes[1] );
                }
                clicked_nodes = [];
            }

        };

        // Helper to get a node by node ID
        var findNode = function (id) {
            for (var i in graph_data.nodes) {
                if (graph_data.nodes[i]["id"] === id) return graph_data.nodes[i];
            }
            ;
        };

        // Helpter to get a node index by node ID
        var findNodeIndex = function (id) {
            for (var i = 0; i < graph_data.nodes.length; i++) {
                if (graph_data.nodes[i].id == id) {
                    return i;
                }
            }
            ;
        };

        // set up the D3 visualisation in the specified element
        var w = 800,
            h = 486;

        var color = d3.scale.category10();

        var vis = d3.select("#graph")
                .append("svg:svg")
                .attr("id", "svg")
                .attr("pointer-events", "all")
                .attr("viewBox", "0 0 " + w + " " + h)
                .attr("perserveAspectRatio", "xMinYMid")
                .append('svg:g');

        var force = d3.layout.force()
                    .nodes(graph_data.nodes)
                    .links(graph_data.links);

        // Update the graph after every action like addNode, addLink, etc
        var update = function () {

            // Add link lines
            if ( graph_data.links.length > 0 ) {
                var link = vis.selectAll("line")
                        .data(graph_data.links, function (d) {
                            return d.source.id + "-" + d.target.id;
                        });

                link.enter().append("line")
                        .attr("id", function (d) {
                            return d.source.id + "-" + d.target.id;
                        })
                        .attr("class", "link");
            }

            // Create node circles
            var node = vis.selectAll("g.node")
                    .data(graph_data.nodes, function (d) { return d.id; });

            var nodeEnter = node.enter().append("g")
                    .attr("class", "node")
                    .call(force.drag);

            nodeEnter.append("svg:circle")
                    .attr("r", 20)
                    .attr("class", "nodeStrokeClass")
                    .attr("id", function (d) { return d.id; })
                    .attr("fill", function(d) { return color(d.id); })
                    .on( "mouseover", hovered )
                    .on( "click", clicked );

            nodeEnter.append("svg:text")
                    .attr("class", "textClass")
                    .attr("x", 22)
                    .attr("y", ".31em")
                    .text(function (d) { return d.id; });

            node.exit().remove();

            // Handle drag events to update new positions of nodes
            force.on("tick", function () {

                node.attr("transform", function (d) {
                    return "translate(" + d.x + "," + d.y + ")";
                });

                if ( graph_data.links.length > 0 ) {

                    link.attr("x1", function (d) {
                        return d.source.x;
                    })
                        .attr("y1", function (d) {
                            return d.source.y;
                        })
                        .attr("x2", function (d) {
                            return d.target.x;
                        })
                        .attr("y2", function (d) {
                            return d.target.y;
                        });
                }


            });

            // Restart the force layout.
            force
                .gravity(.01)
                .charge(-80000)
                .friction(0)
                .linkDistance(200)
                .size([w, h])
                .start();
        };


        // Make it all go
        update();

    }

}());

