!(function(){
    "use strict";
    
    var global_map;
    var gml_data;
    main();

    /**
     * Main function of the script
     */
    function main() {
        // Create the map
        createMap();

        // If we got the JSON filename, load the map
        if ( json_filename.length > 0 ) {
            // Populate the Map with the new JSON file
            d3.json( json_filename, function(data) {
                gml_data = data;
                populateMap();
                populateDataArray();
            });
        } else {
            // Load a default network
            d3.json( '/static/json/Renater2010.gml.json', function(data) {
                gml_data = data;
                populateMap();
                populateDataArray();
            });
        }

    }

    /**
     * Just create the Leaflet Map
     */
    function createMap() {
        // Always update the inner HTML of the map in order to reload a new map if we load a new GML file
        document.getElementById('map-container').innerHTML = "<div id='mapid' style='width: auto; height: 600px;'></div>";
        global_map = L.map('mapid');

        // Select the map provider
        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
            maxZoom: 18,
            attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
            '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery © <a href="http://mapbox.com">Mapbox</a>',
            id: 'mapbox.streets'
        }).addTo(global_map);
    }

    /**
     * Populate the map :
     * - Center the map to the first node
     * - Add a marker for each node
     * - Add all links
     * - Add nodes and links tooltips
     */
    function populateMap() {
        // Put the center of the map to the first node
        global_map.setView([gml_data.nodes[0].Latitude, gml_data.nodes[0].Longitude], 7);

        // For each node, add a Circle on the good latitude&longitude
        gml_data.nodes.forEach(function(d) {
            if ( d.hasOwnProperty('Latitude') && d.hasOwnProperty('Longitude') ) {
                var marker = L.marker([d.Latitude, d.Longitude], 10000, {
                    color: 'red',
                    fillColor: '#f03',
                    fillOpacity: 1
                }).addTo(global_map).bindTooltip(d.id).openTooltip();
            }
        });

        // For each link, add a line between two nodes
        gml_data.links.forEach(function(d) {

            var source_node = null; 
            var target_node = null; 
            gml_data.nodes.forEach(function(n) {
                if ( d.source === n.id ) {
                    source_node = n;
                }
                if ( d.target === n.id ) {
                    target_node = n;
                }
            });

            if ( source_node != null && target_node != null ) {
                if ( source_node.hasOwnProperty('Latitude') && source_node.hasOwnProperty('Longitude') && target_node.hasOwnProperty('Latitude') && target_node.hasOwnProperty('Longitude') ) {
                    var polyline = L.polyline([
                            [source_node.Latitude, source_node.Longitude],
                            [target_node.Latitude, target_node.Longitude]
                        ],
                        {
                            color: 'red',
                            weight: 10,
                            opacity: .7,
                            dashArray: '20,15',
                            lineJoin: 'round'
                        }
                    ).addTo(global_map).bindTooltip(d.LinkLabel).openTooltip();
                }
            }
        })

    }

    /**
     * Add GML data to a readable table
     *
     * @param data
     */
    function populateDataArray() {
        var nodes = gml_data.nodes;
        var nodesTable = $("<table id='nodesTable' class='table table-hover table-bordered table-striped'></table>");
        nodesTable[0].border = "1";
        var columns = Object.keys(nodes[0]);
        var columnCount = columns.length;
        var row = $(nodesTable[0].insertRow(-1));
        for (var i = 0; i < columnCount; i++) {
            var headerCell = $("<th />");
            headerCell.html([columns[i]]);
            row.append(headerCell);
        }

        for (var i = 0; i < nodes.length; i++) {
            row = $(nodesTable[0].insertRow(-1));
            for (var j = 0; j < columnCount; j++) {
                var cell = $("<td />");
                cell.html(nodes[i][columns[j]]);
                row.append(cell);
            }
        }

        var links = gml_data.links;
        var linksTable = $("<table id='linksTable' class='table table-hover table-bordered table-striped'></table>");
        linksTable[0].border = "1";
        var columns = Object.keys(links[0]);
        var columnCount = columns.length;
        var row = $(linksTable[0].insertRow(-1));
        for (var i = 0; i < columnCount; i++) {
            var headerCell = $("<th />");
            headerCell.html([columns[i]]);
            row.append(headerCell);
        }

        for (var i = 0; i < links.length; i++) {
            row = $(linksTable[0].insertRow(-1));
            for (var j = 0; j < columnCount; j++) {
                var cell = $("<td />");
                cell.html(links[i][columns[j]]);
                row.append(cell);
            }
        }

        var dvNodesTable = $("#nodes-table-container");
        var dvLinksTable = $("#links-table-container");
        dvNodesTable.html("");
        dvLinksTable.html("");
        dvNodesTable.append(nodesTable);
        dvLinksTable.append(linksTable);

    }

}());

