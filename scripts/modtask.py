import os, sys

# Importation des logins et variables path
config_path = os.path.abspath( os.path.dirname( getattr(sys.modules['__main__'], '__file__') ) )
if config_path.find( 'scripts') != -1 :
	config_path = os.path.dirname( config_path )
execfile(config_path + '/config.py')

# On compte le nombre d arguments
nbparam = len(sys.argv) - 1

if nbparam != 14:
	print ("Erreur de syntaxe")
	print ("python modtask.py <OLD IP> <OLD SCRIPT> <OLD M> <OLD H> <OLD DOM> <OLD MON> <OLD DOW> <NEW IP> <NEW SCRIPT> <NEW M> <NEW H> <NEW DOM> <NEW MON> <NEW DOW>")
	print ("Exemple : python modtask.py 172.16.0.100 hello.sh 48 0 5 6 3 172.16.0.101 routeron.sh 54 2 7 2 1")
else:
	# Variables
	old_host = sys.argv[1]
	old_script = scriptpath + sys.argv[2]
	old_m = sys.argv[3] # 0-59 - minute
	old_h = sys.argv[4] # 0-23 - heure
	old_dom = sys.argv[5] # 1-31 - jour du mois
	old_mon = sys.argv[6] # 1-12 - mois
	old_dow = sys.argv[7] # 0-6, 0 = dimanche - jour de la semaine
	old_task = old_m + ' ' + old_h + ' ' + old_dom + ' ' + old_mon + ' ' + old_dow + ' python ' + scriptpath + 'launcher.py ' + old_host + ' ' + old_script + ' >/dev/null 2>&1'
	host = sys.argv[8]
	script = scriptpath + sys.argv[9]
	m = sys.argv[10] # 0-59 - minute
	h = sys.argv[11] # 0-23 - heure
	dom = sys.argv[12] # 1-31 - jour du mois
	mon = sys.argv[13] # 1-12 - mois
	dow = sys.argv[14] # 0-6, 0 = dimanche - jour de la semaine
	task = m + ' ' + h + ' ' + dom + ' ' + mon + ' ' + dow + ' python ' + scriptpath + 'launcher.py ' + host + ' ' + script + ' >/dev/null 2>&1'

	# On ajoute la nouvelle tache si elle n existe pas
	result = subprocess.check_output('if grep -q \"' + task + '\" ' + tmppath + 'tasklist.txt; then echo 1; else (crontab -l 2>/dev/null; echo \"' + task + '\") | crontab -; fi', shell=True).strip()
 
	# Si la nouvelle tache est cree, on supprime l ancienne
	if result != 1:
		cmd = 'crontab -l 2>/dev/null | grep -n \"' + old_task + '\" | awk -F: \'{print $1}\''
		result = subprocess.check_output(cmd, shell=True).strip()
		os.system('crontab -l 2>/dev/null | sed \'' + result + 'd\' | crontab -')

		# On met a jour la liste de tache dans le fichier tampon
		execfile(scriptpath + "gettasks.py")
