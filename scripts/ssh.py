import os, sys

# Importation des logins et variables path
config_path = os.path.abspath( os.path.dirname( getattr(sys.modules['__main__'], '__file__') ) )
if config_path.find( 'scripts') != -1 :
	config_path = os.path.dirname( config_path )
execfile(config_path + '/config.py')

# IMPORTANT ! CE SCRIPT EST UNE TENTATIVE DE CENTRALISATION DES OUVERTURES DE CONNEXION SSH
# CELA PERMETTRAIT D EVITER DES CONNEXIONS INUTILES
# TOUTEFOIS IL SEMBLERAIT QUE PXSSH NE PERMET PAS DE SAVOIR SI UNE CONNEXION EST TOUJOURS ACTIVE
# D AILLEURS UN WHO SUR LA MACHINE DISTANTE APRES UNE CONNEXION PXSSH NE DETECTE AUCUNE CONNEXION ACTIVE
# ON CONSEQUENCE CE SCRIPT EST DEPRECIE

# On ouvre une connexion SSH sur chaque machine en ligne
with open(tmppath + 'nodestates.json', 'r') as json_data:
	nodes = json.load(json_data)
	ssh_cons = {
			'connected': [],
			'failed': []
		}
	for node in nodes["nodes"]:
		con_id = node['id'] + "_con"
		con = {
			'con_id': con_id,
			'node_id': node['id'],
                        'node_ip': node['ip']
		}
		try:
			exec(con_id + " = pxssh.pxssh()")
			exec(con_id + ".SSH_OPTS = \"%s -o 'StrictHostKeyChecking=no'\" % " + con_id + ".SSH_OPTS")
                	exec(con_id + ".SSH_OPTS = \"%s -o 'UserKnownHostsFile /dev/null'\" % " + con_id + ".SSH_OPTS")
                	exec(con_id + ".login(\"" + node['ip'] + "\",\"" + user + "\",\"" + password + "\")")
		except:
			ssh_cons['failed'].append(con)
		finally:
			if con not in ssh_cons['failed']:
				ssh_cons['connected'].append(con)

# On export ssh_cons vers un fichier json
with open(tmppath + 'ssh_cons.json', 'w') as outfile:  
	json.dump(ssh_cons, outfile)
