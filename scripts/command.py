import os, sys

# Importation des logins et variables path
config_path = os.path.abspath( os.path.dirname( getattr(sys.modules['__main__'], '__file__') ) )
if config_path.find( 'scripts') != -1 :
	config_path = os.path.dirname( config_path )
execfile(config_path + '/config.py')

# On compte le nombre d arguments
nbparam = len(sys.argv) - 1

if nbparam != 2:
        print ("Erreur de syntaxe")
        print ("python command.py <IP MACHINE> <COMMANDE>")
	print ("<COMMANDE> = control >> pour le controle de la machine distante") 
        print ("Exemple : python command.py 172.16.0.100 \"ifconfig -a\"")
else:
	# Variables
	host = sys.argv[1]
	command = sys.argv[2]

	# On ouvre les fichiers result.txt et errors.txt pour ecrire les eventuels resultats ou erreurs
        result = open(tmppath + 'result.txt', 'w')
        err = open(tmppath + 'errors.txt', 'w')

	# On execute la commande sur la machine distante
	try:
		s.login(host,user,password)
		if command != "control":
			s.sendline(command)
                	s.prompt()
			result.write(s.before)
		else:
			print "CTRL + D pour quitter le mode interaction"
			s.interact()
	except pxssh.ExceptionPxssh as e:
        	err.write("pxssh couldnt log on " + host + "\n")
        	err.write(str(e) + "\n")

	# On ferme les fichiers ouverts
	result.close()
	err.close()

	# On affiche le contenu des fichiers pour tester
	os.system('cat ' + tmppath + 'result.txt')
	os.system('cat ' + tmppath + 'errors.txt')
