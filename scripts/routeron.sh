#!/bin/bash

# On active le mode routeur sur le noeud
sed -i '/net.ipv4.ip_forward=1/s/^#//g' /etc/sysctl.conf
sysctl -p /etc/sysctl.conf
