import os, sys

# Importation des logins et variables path
config_path = os.path.abspath( os.path.dirname( getattr(sys.modules['__main__'], '__file__') ) )
if config_path.find( 'scripts') != -1 :
	config_path = os.path.dirname( config_path )
execfile(config_path + '/config.py')

# On supprime toutes les taches planifiees existantes :
os.system('crontab -r 2>/dev/null')

# On met a jour la liste de tache dans le fichier tampon
execfile(scriptpath + "gettasks.py")
