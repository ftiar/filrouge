# POUR RECUPERER LA LISTE DES IP DISTRIBUEES PAR LE DHCP
# python leases.py

# POUR RECUPERER L ETAT DES NOEUDS
# python nodestates.py

# POUR EXECUTER UN SCRIPT SUR UNE MACHINE
# python launcher.py <IP MACHINE> <NOM SCRIPT> <ARG1> <ARG2> .. <ARGn>
# Exemple : python launcher.py 172.16.0.100 hello.sh

# POUR AJOUTER UN VLAN SUR UNE MACHINE
# python launcher.py <IP MACHINE> addvlan.sh <INTERFACE> <IDVLAN> <IPSR>
# Exemple python launcher.py 172.16.0.100 addvlan.sh eth1 10 192.168.0.1/24
# USE CASE 1 : Creer un lien reviens a ajouter un VLAN identique avec des IP dans le meme reseau sur 2 machines
# USE CASE 2 : Creer un switch reviens a ajouter un VLAN identique avec des IP dans le meme reseau pour 3 machines ou plus
# USE CASE 3 : Creer un routeur reviens a ajouter plusieurs VLAN avec IP differentes sur une machine puis d activer le routage avec "routeron.sh"

# POUR SUPPRIMER UN VLAN SUR UNE MACHINE
# python launcher.py <IP MACHINE> rmvlan.sh <INTERFACE> <IDVLAN>
# Exemple python launcher.py 172.16.0.100 rmvlan.sh eth1 10

# POUR PLANIFIER L EXECUTION D UNE TACHE
# python addtask.py <IP MACHINE> <NOM SCRIPT> <M> <H> <DOM> <MON> <DOW>
# Exemple : python addtask.py 172.16.0.100 hello.sh 25 23 3 6 0

# POUR MODIFIER UNE TACHE PLANIFIEE
# python modtask.py <OLD IP> <OLD SCRIPT> <OLD M> <OLD H> <OLD DOM> <OLD MON> <OLD DOW> <NEW IP> <NEW SCRIPT> <NEW M> <NEW H> <NEW DOM> <NEW MON> <NEW DOW>
# Exemple : python modtask.py 172.16.0.100 hello.sh 48 0 5 6 3 172.16.0.101 routeron.sh 54 2 7 2 1

# POUR SUPPRIMER UNE TACHE PLANIFIEE
# python deltask.py <IP MACHINE> <NOM SCRIPT> <M> <H> <DOM> <MON> <DOW>
# Exemple : python deltask.py 172.16.0.100 hello.sh 25 23 3 6 0

# POUR RECUPERER LA LISTE DES TACHES PLANIFIEES
# python gettasks.py

# POUR SUPPRIMER TOUTES LES TACHES PLANIFIEES (RESET)
# python resettasks.py

# POUR ENVOYER UNE COMMANDE SUR UNE MACHINE DISTANTE OU EN PRENDRE SON CONTROLE : 
# python command.py <IP MACHINE> <COMMANDE> (si command = "control" alors l utilisateur prend le controle de la machine)
# Exemple : python command.py 172.16.0.100 "ifconfig -a"

### CONTENU DES FICHIERS TEMPORAIRES ###
# "leases.txt" contient toutes IP distribuees par le DHCP.
# "nodesonline.txt" contient le nombre de noeuds en ligne.
# "nodestates.txt" contient l etat des noeuds en ligne (IP, MAC et LATENCE).
# "nodestates.json" est identique au ficher "nodestates.txt" mais au format JSON pour les dev.
# "result.txt" contient le resultat de l execution d un script ou d une commande sur une machine distante.
# "errors.txt" contient les erreurs possibles lors de l execution d un script ou d une commande sur une machine distante.
# "tasklist.txt" contient la liste des taches planifiees sur le serveur.
# "ssh_cons.json" contient la liste des connexions SSH actives sur les machines distantes au format JSON pour les dev. Toutefois il est deprecie.
