#!/bin/bash

# $1 = interface (eth0, etc)
# $2 = vlan (10, etc)
# $3 = ip sous interface (192.168.0.10/24)

# On active dot1q
echo 8021q >> /etc/modules
modprobe 8021q

# On install le paquet VLAN s il n est pas installe
dpkg-query -W -f='${Status}\n' vlan >tmp 2>&1
res=$(cat tmp)
if [ "$res" != "install ok installed" ]
then
        apt-get update
        apt-get install vlan
fi
rm tmp

# On ajoute une sous interface dot1q
vconfig add $1 $2

# On cree l interface reseau de la sous interface
# Attention l interface est joignable par un autre hote uniquement si un switch compatible VLAN (trunk, tag) est entre les deux
# Entre VM sur un meme hote physique, on peut tester la communication en specifiant l interface de sortie
# Exemple : ping 192.168.0.2 -I eth1
ifconfig $1.$2 $3

# Dans certain cas il est necessaire de configurer la MTU sur 1496
# ifconfig $1.$2 mtu 1496

# Commandes VRF (pour creer une table de routage par VLAN)

# Creer une table de routage
# echo 10    vlan10 >> /etc/iproute2/rt_tables

# Creer un routage statique
# ip route add 192.168.0.0/24 table vlan10 dev eth1.10 proto static
# ip route add default table vlan10 dev eth1.10 via 192.168.0.254 proto static

# Ajouter les regles d association du trafic entrant et des tables de routages
# ip rule add iif eth1.10 table vlan10 prio 1010
