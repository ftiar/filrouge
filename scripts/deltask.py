import os, sys

# Importation des logins et variables path
config_path = os.path.abspath( os.path.dirname( getattr(sys.modules['__main__'], '__file__') ) )
if config_path.find( 'scripts') != -1 :
	config_path = os.path.dirname( config_path )
execfile(config_path + '/config.py')

# On compte le nombre d arguments
nbparam = len(sys.argv) - 1

if nbparam != 7:
	print ("Erreur de syntaxe")
	print ("python deltask.py <IP MACHINE> <NOM SCRIPT> <M> <H> <DOM> <MON> <DOW>")
	print ("Exemple : python deltask.py 172.16.0.100 hello.sh 25 23 3 6 0")
else:
	# Variables
	host = sys.argv[1]
	script = scriptpath + sys.argv[2]
	m = sys.argv[3] # 0-59 - minute
	h = sys.argv[4] # 0-23 - heure
	dom = sys.argv[5] # 1-31 - jour du mois
	mon = sys.argv[6] # 1-12 - mois
	dow = sys.argv[7] # 0-6, 0 = dimanche - jour de la semaine
	task = m + ' ' + h + ' ' + dom + ' ' + mon + ' ' + dow + ' python ' + scriptpath + 'launcher.py ' + host + ' ' + script + ' >/dev/null 2>&1'

	# On cherche la ligne contenant la tache
	cmd = 'crontab -l 2>/dev/null | grep -n \"' + task + '\" | awk -F: \'{print $1}\''
	result = subprocess.check_output(cmd, shell=True).strip()

	# On supprime la tache :
	os.system('crontab -l 2>/dev/null | sed \'' + result + 'd\' | crontab -')

	# On met a jour la liste de tache dans le fichier tampon
	execfile(scriptpath + "gettasks.py")
