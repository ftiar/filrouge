import os, sys

# Importation des logins et variables path
config_path = os.path.abspath( os.path.dirname( getattr(sys.modules['__main__'], '__file__') ) )
if config_path.find( 'scripts') != -1 :
	config_path = os.path.dirname( config_path )
execfile(config_path + '/config.py')

# On compte le nombre d arguments
nbparam = len(sys.argv) - 1

if nbparam < 2:
	print ("Erreur de syntaxe")
	print ("python launcher.py <IP MACHINE> <NOM SCRIPT> <ARG1> <ARG2> .. <ARGn>")
	print ("Exemple : python launcher.py 172.16.0.100 hello.sh")
elif nbparam >= 2:
	# Variables communes
	host = sys.argv[1]
	script = sys.argv[2]

	if nbparam > 2:
		param = {}
		var = "ARG"
		advscript = script
		def fonction(n, value):
			param[var+str(n)] = value
		# Variables positionnelles pour certain scripts (addvlan.sh, etc)
		for i in range(nbparam-2):
			fonction(i+1, sys.argv[i+3])
			advscript = advscript + " " + param[var+str(i+1)]
	
	# On ouvre les fichiers result.txt et errors.txt pour ecrire les eventuels resultats ou erreurs
	result = open(tmppath + 'result.txt', 'w')
	err = open(tmppath + 'errors.txt', 'w')

	# On copie le script sur la machine distante
	try:
		child = pexpect.spawn('scp -q ' + scriptpath + script + ' ' + user + '@' + host + ':' + remotepath)
		child.expect ('assword:')
		child.sendline (password)
		child.expect(pexpect.EOF)
	except Exception as e:
		err.write("failed to copy " + script + " to " + host + "\n")
		err.write(str(e) + "\n")
	
	# On execute le script sur la machine distante
	try:
		s.login(host,user,password)
		s.sendline('chmod u+x ' + script)
		s.prompt()
		if nbparam == 2:
			s.sendline('./' + script)
		else:
			s.sendline('./' + advscript)
		s.prompt()
		result.write(s.before)
		s.sendline('rm ' + script)
		s.prompt()
	except pxssh.ExceptionPxssh as e:
		err.write("pxssh couldnt log on " + host + "\n")
		err.write(str(e) + "\n")

	# On ferme les fichiers ouverts
	result.close()
	err.close()

	# On affiche le contenu des fichiers pour tester
	os.system('cat ' + tmppath + 'result.txt')
	os.system('cat ' + tmppath + 'errors.txt')
