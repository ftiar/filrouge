#!/bin/bash

if [ $# -ne 1 ]
then
	echo "Syntaxe : ./qos.sh \$ETH_CLIENT"
	echo "     \$ETH_CLIENT : nom de l'interface côté client"
fi

ETH_CLIENT=$1

# Effacer la config précédente
tc qdisc del dev $ETH_CLIENT root
iptables -t mangle -F

# Création d'une nouvelle politique de gestion des priorités pour 
# l'interface côté client
tc qdisc add dev $ETH_CLIENT root handle 1: htb

# Ajout d'une classe pour l'ensemble du trafic : pas plus de 100 Mbps
tc class add dev $ETH_CLIENT parent 1: classid 1:1 htb rate 100mbit ceil 100mbit

# Ajout d'une classe pour UDP
tc class add dev $ETH_CLIENT parent 1:1 classid 1:10 htb rate 35mbit ceil 35mbit
# Ajout d'une classe pour la video sur UDP
tc class add dev $ETH_CLIENT parent 1:10 classid 1:11 htb rate 34mbit ceil 35mbit
# Ajout d'une classe pour le reste du trafic UDP
tc class add dev $ETH_CLIENT parent 1:10 classid 1:12 htb rate 1mbit ceil 35mbit

# Question 11
# Ajout d'une classe pour TCP
tc class add dev $ETH_CLIENT parent 1:1 classid 1:20 htb rate 65mbit ceil 65mbit
# Ajout d'une classe pour le web
tc class add dev $ETH_CLIENT parent 1:20 classid 1:21 htb rate 40mbit ceil 60mbit
# Ajout d'une classe pour P2P
tc class add dev $ETH_CLIENT parent 1:20 classid 1:22 htb rate 10mbit ceil 10mbit
# Ajout d'une classe pour le reste du trafic TCP
tc class add dev $ETH_CLIENT parent 1:20 classid 1:23 htb rate 15mbit ceil 60mbit

# Marquage des paquets TCP pour les mettre dans la classe 1:23
iptables -t mangle -A POSTROUTING -o $ETH_CLIENT -p tcp -j CLASSIFY --set-class 1:23
# Marquage des paquets TCP sur le port 80 (web) pour les mettre dans la classe 1:21
iptables -t mangle -A POSTROUTING -o $ETH_CLIENT -p tcp --dport 80 -j CLASSIFY --set-class 1:21
# Marquage des paquets TCP sur le port 6881 (video VLC) pour les mettre dans la classe 1:22
iptables -t mangle -A POSTROUTING -o $ETH_CLIENT -p tcp --dport 6881 -j CLASSIFY --set-class 1:22
# Marquage des paquets UDP pour les mettre dans la classe 1:12
iptables -t mangle -A POSTROUTING -o $ETH_CLIENT -p udp -j CLASSIFY --set-class 1:12
# Marquage des paquets UDP sur le port 1234 (video VLC) pour les mettre dans la classe 1:11
iptables -t mangle -A POSTROUTING -o $ETH_CLIENT -p udp --dport 1234 -j CLASSIFY --set-class 1:11


# Question 10 :
# Quelle partie du schéma est déjà mise en œuvre dans ce script ?
# - La classe principale (Tout le trafic) représente l’ensemble du trafic sur l’interface côté client. 
# - UDP, qui a son tour englobe deux classes filles : l’une pour le trafic vidéo VLC, l’autre pour le reste du trafic UDP
