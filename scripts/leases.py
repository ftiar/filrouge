import os, sys

# Importation des logins et variables path
config_path = os.path.abspath( os.path.dirname( getattr(sys.modules['__main__'], '__file__') ) )
if config_path.find( 'scripts') != -1 :
	config_path = os.path.dirname( config_path )
execfile(config_path + '/config.py')

# On recupere les IP distribuees par le DHCP
os.system('cat /var/lib/dhcp/dhcpd.leases | grep -v \"^#\" | grep \"^lease\" | cut -d \" \" -f2 | sort | uniq > ' + tmppath + 'leases.txt')
