import os, sys

# Importation des logins et variables path
config_path = os.path.abspath( os.path.dirname( getattr(sys.modules['__main__'], '__file__') ) )
if config_path.find( 'scripts') != -1 :
	config_path = os.path.dirname( config_path )
execfile(config_path + '/config.py')

# On recupere l etat des noeuds
os.system("nmap -sn -n -iL " + tmppath + "leases.txt | awk '/report|latency|MAC/ {gsub(/Nmap scan report for |Host is up \(| latency\).|MAC Address: | \(Cadmus Computer Systems\)| \(VMware\)/,\"\"); print}' | xargs -d '\n' -n 3 | tr \" \" \";\" > " + tmppath + "nodestates.txt")

# On recupere le nombre de noeuds en ligne
os.system("cat " + tmppath + "nodestates.txt | grep -c \"^172.16.0.\" > " + tmppath + "nodesonline.txt")
result = subprocess.check_output('cat ' + tmppath + 'nodesonline.txt', shell=True).strip()

# Si des noeuds sont en ligne
if result != "0":
	# On convertit pour json
	nodes = {}
	nodes["nodes"] = []
	with open(tmppath + 'nodestates.txt', 'r') as nsFile:
                for index, line in enumerate(nsFile, 1):
			line = line.strip() 
                	node = line.split( ";" )
                	nodes["nodes"].append({  
                        	'id': "node" + str(index),
                        	'ip': node[0],
                        	'latence': node[1],
                        	'mac': node[2]
                	})
	
	# Export to the json file
	with open(tmppath + 'nodestates.json', 'w') as outfile:  
		json.dump(nodes, outfile)
