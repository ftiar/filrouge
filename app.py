#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import print_function
import sys
import os
from flask import Flask, session, request, flash, redirect, render_template, jsonify, url_for, send_from_directory
from werkzeug.utils import secure_filename
import networkx as nx
from networkx.readwrite import json_graph
import requests
import simplejson 
import json
import glob
import flask_login
import urllib
import datetime
from pprint import pprint
from math import ceil

ALLOWED_EXTENSIONS = set(['gml'])
abspath = os.path.abspath( os.path.dirname( getattr(sys.modules['__main__'], '__file__') ) )
GML_UPLOAD_FOLDER = abspath + '/static/gml'
JSON_UPLOAD_FOLDER = abspath + '/static/json/'
SCRIPTS_FOLDER = abspath + '/scripts/'
TMP_FOLDER = abspath + '/static/network/'

app = Flask(__name__)
app.secret_key = 'esi5_redwire'
app.config['GML_UPLOAD_FOLDER'] = GML_UPLOAD_FOLDER
app.config['JSON_UPLOAD_FOLDER'] = JSON_UPLOAD_FOLDER
app.config['ABSPATH'] = abspath

login_manager = flask_login.LoginManager()
login_manager.init_app(app)
users = {
    'contact@tiar-florian.fr': {'password': 'floflo'},
    'admin': {'password': 'ingetis2018'}
}
PER_PAGE = 9 # Pagination

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() \
        in ALLOWED_EXTENSIONS

def url_for_other_page(page):
    args = dict(request.view_args.items() + request.args.to_dict().items()) 
    args['page'] = page
    return url_for(request.endpoint, **args)
app.jinja_env.globals['url_for_other_page'] = url_for_other_page

def convertGMLtoJSON( filename ):
    try:
        g = nx.read_gml(app.config['GML_UPLOAD_FOLDER'] + '/' + filename)
        d = json_graph.node_link_data(g)
        with open(app.config['JSON_UPLOAD_FOLDER'] + filename
              + '.json', 'w') as fp:
            json.dump(d, fp)
        return jsonify(filepath=filename + '.json')
    except:
        print('error')
        return 'error'

def getAllTopologies():
    with open( abspath + "/static/zoo_topologies_objects.json" ) as f:
        topologies = json.load(f)
        topologies = sorted( topologies["topologies"] )
    return topologies

def getTopology( name ):
    topologies = getAllTopologies()
    for topo in topologies:
        if topo["name"] == name:
            return topo
    return ""

def generateJSONtopologies():
    topologies = {}  
    topologies['topologies'] = []  

    for json_filepath in glob.glob( JSON_UPLOAD_FOLDER + "*.json"):
        with open( json_filepath ) as json_data:
            data = json.load(json_data)

            topologies["topologies"].append({  
                'name': data["graph"]["label"],
                'json': json_filepath,
                'image': ZOO_TOPOLOGY_IMG_URL + data["graph"]["label"] + ".jpg",
                'geo_location': data["graph"]["GeoLocation"],
                'geo_extent': data["graph"]["GeoExtent"],
                'date': data["graph"]["DateObtained"]
            })

    filename = abspath + "/static/zoo_topologies_objects.json"
    with open(filename, 'w') as fp:
        json.dump(topologies, fp)

    return filename

@app.route('/getAllBashScripts', methods=['POST'])
def getAllBashScripts():
    os.chdir(SCRIPTS_FOLDER)
    return jsonify( glob.glob("*.sh") )

@app.route('/deployScript', methods=['POST'])
def deployScript():
    if request.method == 'POST':
        node = request.form['node']
        script = request.form['script']

        if script and node:
            os.chdir(SCRIPTS_FOLDER)
            print( "python launcher.py " + node + " " + script )
            os.system( "python launcher.py " + node + " " + script )
            return jsonify('ok')
    return jsonify('ko')

@app.route('/exportJSONtoGML', methods=['POST'])
def exportJSONtoGML():
    if request.method == 'POST':
        json_obj = json.loads( request.form['json_obj'] )

        if json_obj:
            graph = json_graph.node_link_graph(json_obj)
            os.chdir(GML_UPLOAD_FOLDER)
            now = datetime.datetime.now()
            new_filename = "user_export_" + now.strftime("%Y-%m-%d_%Hh%M") + ".gml"
            nx.write_gml(graph, GML_UPLOAD_FOLDER + "/" + new_filename)
            return new_filename
        else:
            return 'Json object misformated'
    else:
        return 'Wrong method; please use POST'

@app.route('/', defaults={'page': 1})
@app.route('/page/<int:page>')
def home( page ):
    topologies = getAllTopologies()
    if not topologies and page != 1:
        abort(404)
    count = len( topologies )

    start_page = PER_PAGE * ( page - 1 )
    end_page = PER_PAGE * page
    
    topo = topologies[start_page:end_page]

    pagination = Pagination(page, PER_PAGE, count)

    return render_template('home.html', pagination=pagination, topologies=topo)

@app.route( '/single_topology/<topo>', methods=['GET'] )
def single_topology( topo ):
    
    topology = getTopology( topo )
    if not topology:
        abort(404)
    
    return render_template('single_topology.html', topo=topology)

@app.route( '/single_topology_uploaded_gml', methods=['POST'] )
def single_topology_uploaded_gml():    
    if request.method == 'POST':
        file = request.files['gml_file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['GML_UPLOAD_FOLDER'], filename))
            convertGMLtoJSON( filename )
            filename = "/static/json/" + filename + ".json"

    pprint( filename )
    return render_template('single_topology_uploaded_gml.html', file=filename)

@app.route('/create_topology')
@flask_login.login_required
def create_topology():
    execfile(SCRIPTS_FOLDER + "leases.py")
    execfile(SCRIPTS_FOLDER + "nodestates.py")

    return render_template('create_topology.html', user=flask_login.current_user.id, script_path=SCRIPTS_FOLDER)

@app.route('/uploads/<path:filename>', methods=['GET', 'POST'])
def download(filename):
    return send_from_directory(directory=GML_UPLOAD_FOLDER, filename=filename)

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html')

    email = request.form['email']
    is_good_mail = users.get(email, "")
    if  is_good_mail and request.form['password'] == users[email]['password']:
        user = User()
        user.id = email
        flask_login.login_user(user)
        return redirect(url_for('create_topology'))
    return "Erreur : mot de passe ou adresse email incorrect"

@app.route('/logout')
def logout():
    flask_login.logout_user()
    session.pop('username', None)
    return redirect(url_for('home'))

#
#
#### USER Class
#
#
class User(flask_login.UserMixin):
    pass

@login_manager.user_loader
def user_loader(email):
    if email not in users:
        return

    user = User()
    user.id = email
    return user

@login_manager.request_loader
def request_loader(request):
    email = request.form.get('email')
    if email not in users:
        return

    user = User()
    user.id = email

    user.is_authenticated = request.form['password'] == users[email]['password']

    return user

@login_manager.unauthorized_handler
def unauthorized_handler():
    return redirect(url_for('login'))


#
#
#### Pagination Class
#
#
class Pagination(object):

    def __init__(self, page, per_page, total_count):
        self.page = page
        self.per_page = per_page
        self.total_count = total_count

    @property
    def pages(self):
        return int(ceil(self.total_count / float(self.per_page)))

    @property
    def has_prev(self):
        return self.page > 1

    @property
    def has_next(self):
        return self.page < self.pages

    def iter_pages(self, left_edge=2, left_current=2,
                   right_current=5, right_edge=2):
        last = 0
        for num in xrange(1, self.pages + 1):
            if num <= left_edge or \
               (num > self.page - left_current - 1 and \
                num < self.page + right_current) or \
               num > self.pages - right_edge:
                if last + 1 != num:
                    yield None
                yield num
                last = num



if __name__ == '__main__':
    app.run(debug=True)
      